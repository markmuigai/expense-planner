import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  // Text of the weekday
  final String label;

  // Amount spent
  final double spendingAmount;

  // spending percentage
  final double spendingPctOfTotal;

  // Constructor
  ChartBar(this.label, this.spendingAmount, this.spendingPctOfTotal);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        // To string as fixed to remove all decimal places i.e rounded off
        // fitted box scales children within itself, also shrinks text
        Container(
          // Text will always have a height of 20
          height: 20,
          child: FittedBox(
            child: Text('\$${spendingAmount.toStringAsFixed(0)}')
          ),
        ),
        SizedBox(height: 4),
        // Main Bar
        Container(
          height: 60,
          width: 10,
          // Bakcground that is filled based on amount for the day
          // Stack widget places widgets on top of each other, i.e can overlap unlike a column
          child: Stack(
            children: <Widget>[
              // Inner layer
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.grey, width: 1.0
                    ),
                    // Background color
                  color:  Color.fromRGBO(220, 220, 220, 1),
                  // Rounded corners
                  borderRadius: BorderRadius.circular(10) 
                ),
              ),
              // Outer layer to show expense percentage
              // Special widget that creates a box that is sized as a fraction of a value  
              // Height factor should be a value of 0 or 1
              FractionallySizedBox(
                heightFactor: spendingPctOfTotal,
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10) 
                  ),
                ),
              ),
            ], 
          ), 
        ),
        SizedBox(
          height:4,
        ),
        Text(
          label
        ),
      ],
    );
  }
}