import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';
import 'chart_bar.dart';

class Chart extends StatelessWidget {
  // All transactions
  final List<Transaction> recentTransactions;

  // Constructor
  Chart(this.recentTransactions);

  // Getter
  List<Map<String, Object>> get groupedTransactionValues {
    // Executes for every generated list element, i.e max 7 times 
    return List.generate(7, (index) {
      // Group by date logic
      // The app generates data for the last 7 weekdays
      // When the index is 0, that means 0 days from today,
      // Then get the todays weekday
      final weekDay = DateTime.now().subtract(
         Duration(days: index),
      );

      // Initialize variable for sum all transactions for this day
      var totalSum = 0.0;

      // Loop through transactions of the previous 7 days
      for (var i = 0; i < recentTransactions.length; i++) {
        // Check if the transaction happened on current day, month,year
        if(recentTransactions[i].date.day == weekDay.day &&
            recentTransactions[i].date.month == weekDay.month &&
            recentTransactions[i].date.year == weekDay.year) {
              totalSum += recentTransactions[i].amount;
          }
      }

      // print(DateFormat.E().format(weekDay));
      // print(totalSum);

      // Assign weekday value to the day and total amount of transactions for that day 
      return {
        // substring(0,1) to show only the first character
        'day': DateFormat.E().format(weekDay).substring(0,1),
        'amount' : totalSum
      };
    }).reversed.toList();
  }

  // Getter for total amount for the week
  double get totalSpending {
    // fold is the equivalent of a reduce function
    // Fold changes a list to a another type based on the logic we pass to the fold function  
    // First argument to fold is the starting value
    // Second argument is the function 
    // Function return a new value which will be added to the starting value
    return groupedTransactionValues.fold(0.0, (sum, item) {
      // return new sum
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    // print(groupedTransactionValues);
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 5
      ),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: groupedTransactionValues.map((data) {
            // Takes a fix argument that control how child elements occupy space 
            return Flexible(
              fit: FlexFit.tight,
              child: ChartBar(data['day'],
                data['amount'],
                // if else shortcode
                totalSpending == 0.0 ? 0.0 : (data['amount'] as double ) / totalSpending),
            );
          }).toList(),
        ),
      ),
    );
  }
}