import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  // Receive transactions
  final List<Transaction> transactions;

  // Delete transaction method
  final Function deleteTransaction;

  // Constructor
  TransactionList(this.transactions, this.deleteTransaction);
    
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      // A list view is a column with a singleChildScrollView 
      // Builder uses a function that takes a context which holds information on the position of the widget in the widget tree
      // and the index of the item to be rendered from the list  
      child: transactions.isEmpty 
      ? Column(
        children: <Widget>[
          Text('No Transactions added Yet', style: Theme.of(
            context).textTheme.title,
          ),
          // Used to provide spacing between elements
          SizedBox(
            height: 10,
          ),
          Container(
            height: 200,
            child: Image.asset(
              'assets/images/waiting.png',
              fit: BoxFit.cover
            ),
          )
        ]
      )
      : ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            margin: EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 5
            ),
            elevation: 5,
            child: ListTile(
              // leading positioned at the start of the widget
              leading: CircleAvatar(
                radius: 30,
                child: Padding(
                    padding: EdgeInsets.all(6),
                    child: FittedBox(
                      child: Text('\$${transactions[index].amount}')
                  ),
                ),
              ),
              title: Text(
                transactions[index].title,
                style: Theme.of(context).textTheme.title,
              ),
              subtitle: Text(
                DateFormat.yMMMd().format(transactions[index].date)
              ),
              // Last element in the widget
              trailing: IconButton(
                icon: Icon(Icons.delete),
                color: Theme.of(context).errorColor,
                // Wrap into anonymous function to pass the reference to the function
                // with arguments
                onPressed: () => deleteTransaction(transactions[index].id)
              ),
            ),
          );
        },
        itemCount: transactions.length,
      ),
    );
  }
}