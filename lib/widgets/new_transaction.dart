import 'package:flutter/material.dart'; 
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  // Store pointer to add transaction method 
  final Function addTransaction;

  // TextEditingController() is provided by dart to assign to text fields 

  NewTransaction(this.addTransaction);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  // Initialize properties
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();

  // is not final since it will change during run time 
  DateTime _selectedDate;

  void _submitData(){
    // Prevent parse from failing
    if(_amountController.text.isEmpty) {
      return;
    }

    // Initialize properties
    final enteredTitle = _titleController.text;
    final enteredAmount = double.parse(_amountController.text);

    // Validation to submit only if all fields have been filled 
    if(enteredTitle.isEmpty || enteredAmount <=0 || _selectedDate == null) {
      return;
    }

    // Use widget. to access properties and methods of my widget class in my state class
    // Add transaction
    widget.addTransaction(
      enteredTitle,
      enteredAmount,
      _selectedDate
    );

    // Close bottom modal after submitting data
    Navigator.of(context).pop();
  }

  // Show datepicker 
  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime.now()
      // .then is a 'future' i.e is a dart feature that waits for a user to pick a value
      // triggered when a user picks a date
    ).then((pickedDate){
      if (pickedDate == null){
        // do NOTHING
        return;
      }

      // Wrap in set state to ensure it is being rebuilt
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            // Receive user input
            TextField(
              decoration: InputDecoration(
                labelText: 'Title'
              ),
              // using controllers saves us from writing a custom function
              // that takes a users value and saves it to a property in the class
              controller: _titleController,
              // Fires on every keystroke
              // Anonymous function takes a users value
              // onChanged: (value) {   
                // Overwrite the value of the class property titleInput with the value
                // passed by the user
              //   titleInput = value;
              // },
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Amount'
              ),
              controller: _amountController,
              keyboardType: TextInputType.number,
              // Enable submission on clicking tick in keyboard
              // Accept an argument but it is not used in the function ?
              onSubmitted: (_) => _submitData(),
              // One expression in function shortcode
              // onChanged: (value) => amountInput = value,
            ),
            // Datepicker
            Container(
              height: 70,
              child: Row(children: <Widget>[
                // Flexible, fit:tight to take as much space as possible
                Expanded(
                  child: Text(_selectedDate == null ? 
                    'No Date Chosen' :
                    'Picked Date: ${DateFormat.yMMMd().format(_selectedDate)}'
                  ),
                ),
                FlatButton(
                  textColor: Theme.of(context).primaryColor,
                  child: Text(
                    'Choose Date',
                    style: TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  onPressed: _presentDatePicker,
                )
              ],),
            ),
            RaisedButton(
              child: Text('Add Transaction'),
              color: Theme.of(context).primaryColor,
              textColor: Theme.of(context).textTheme.button.color,
              onPressed: _submitData,
            ),
          ],
        ),
      ),
    );
  }
}