// Import to have access to the @required operator for required class properties
import 'package:flutter/foundation.dart';

// Class that defines properties of a transaction
class Transaction {
  // Final because the variables are 
  // Runtime constant, i.e they get the value when the transaction is created
  // But the value does not change after
  @required final String id;
  @required final String title;
  @required final double amount;
  @required final DateTime date;

  // Costructor using named arguments
  Transaction({this.id, this.title, this.amount, this.date});
}