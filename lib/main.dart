import 'widgets/new_transaction.dart';
import 'package:flutter/material.dart';
import 'widgets/transaction_list.dart';
import 'models/transaction.dart';
import 'widgets/chart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personal Expenses',
      // Set a global application wide theme
      theme: ThemeData(
        primarySwatch: Colors.purple,
        accentColor: Colors.amber,
        errorColor: Colors.red,
        fontFamily: 'Quicksand',
        textTheme: ThemeData.light().textTheme.copyWith(
          title: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 18,
            fontWeight: FontWeight.bold
          ),
          button: TextStyle(
            color: Colors.white
          )
        ),
        // Define custom app bar theme
        // Use default text theme with
        appBarTheme: AppBarTheme(
          // All text elements in the bar to use the theme
          // Pass arguments to copywith to override the theme
          textTheme: ThemeData.light().textTheme.copyWith(
            title: TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 20,
              fontWeight: FontWeight.bold
            ),
          )
        ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  // Method that opens the form
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Transaction> _userTransactions = [];

  // Dynamically calculated property (getter)
  List<Transaction>get _recentTransactions {
    return _userTransactions.where((transaction) {
      // Check if date is within the past 7 days
      return transaction.date.isAfter(DateTime.now().subtract(
        Duration(days: 7),
        )
      );
    }).toList();
  }

  // Method
  void _addNewTransaction(String transactionTitle, double transactionAmount, DateTime chosenDate){
    final newTransaction = Transaction(
      title: transactionTitle,
      amount: transactionAmount,
      date: chosenDate,
      id: DateTime.now().toString(),  
    );

    // Rebuild transaction list
    setState(() {
      // Add element to existing list
      _userTransactions.add(newTransaction);
    });
  }

  // Show modal with text inputs 
  void _showAddNewTransaction(BuildContext ctx) {
    showModalBottomSheet(
      context: ctx, 
      builder: (_) {
        // Do nothing on taping the modal
        return GestureDetector(
          child: NewTransaction(_addNewTransaction),
          onTap: () {},
          // Manage on tap behaviour
          behavior: HitTestBehavior.opaque,
        );
      }
    );
  }

  // Delete transaction handler
  void _deleteTransaction(String id) {
    // Call setState since UI will need to be rebuilt
    setState(() {
      // Remove object from list that satisfies test
      _userTransactions.removeWhere((transaction) => transaction.id == id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Personal Expenses'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => _showAddNewTransaction(context)
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Chart(_recentTransactions),
            TransactionList(_userTransactions, _deleteTransaction),
          ],
        ),
      ),
      // Change position of floating action button
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _showAddNewTransaction(context)
      ),
    );
  }
}
